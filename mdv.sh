#!/bin/bash
d="$(dirname $0)"
f="$(basename $1)"
t="$(mktemp -d)"
echo "MDV directory : $d"
echo "Temporary working directory : $t"
echo "Input image : $f"

convert $1 -scale 4960 $t/$f.4960.jpeg
convert $1 -scale 9933 $t/$f.9933.jpeg
DISPLAY=:0 $d/pixelize/pixelize -i $t/$f.4960.jpeg -o $d/out/$f.4960.jpeg -w 46 -h 68  -d $d/pixelize/pic_db.dat
DISPLAY=:0 $d/pixelize/pixelize -i $t/$f.9933.jpeg -o $d/out/$f.9933.jpeg -w 92 -h 136 -d $d/pixelize/pic_db.dat
rm -rf $t
echo "Retrieve results in : $d/out"
