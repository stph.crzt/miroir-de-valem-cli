# miroir-de-valem-cli

## INSTALL

### Requirements
- `apt install make build-essential libgtk-3-dev clang-format`
- to be launched pixelize needs a pic_db.dat file created with make_db
- needs a X server server side to execute GTK

### Optionnal
`apt install imagemagick`

## DOC
- `sh miroir-de-valem-cli/mdv.sh in/file.jpeg`
- retrieve results in : `miroir-de-valem-cli/out/`

### Formats

#### General
Reflection = 108 Thumbnails wide (portrait version)

#### Reflection (original Raspberry version)
2700 x 4810 pixels
Thumbnail : 25 x 37

#### A2 300 dpi (printing)
4960 x 7016 pixels
Thumbnail : 46 x 68

#### A0 300 dpi (HR)
9933 x 14043 pixels
Thumbnail : 92 x 136

## SAMPLES
https://pi.crzt.fr/www/tmp/photos

#TODO
- Build real A2 image (add white or black strips to fullfill 4960 x 7016)
- Add a waterprint MDV somewhere
- Manage portrait / lanscape
- Play with contraste ?

## MISC

### Advanced config

In order to have a server side X server on Debian :
https://techoverflow.net/2019/02/23/how-to-run-x-server-using-xserver-xorg-video-dummy-driver-on-ubuntu/

apt install xserver-xorg-video-dummy
create dummy-1920x1080.conf (cf etc)
sudo X -config dummy-1920x1080.conf
run DISPLAY=:0 before command, exemple : DISPLAY=:0 pixelize/pixelize

### CAPITALISATION

#### Known problem with GTK on server (solved ?)
https://framagit.org/stph.crzt/miroir-de-valem-cli/-/blob/master/pixelize/src/make_db.c
if (gtk_init_with_args(&argc, &argv, "[FILE...]", entries, NULL, NULL))
    returns : true if the commandline arguments (if any) were valid and if the windowing system has been successfully initialized, false otherwise

#### Metapixel
Metapixel is an alternative to Pixelize
- https://doc.ubuntu-fr.org/metapixel
- Implements CLI thumbnail width and height
- Distance seems not to work at all (may depends on the size avec the thumbnail database)
- `metapixel-prepare images src`
- `metapixel --library src --width=50 --heigh=74 --metapixel /tmp/test.jpg output2.jpg --cheat=15 --search=local`
